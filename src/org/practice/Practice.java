package org.practice;

public class Practice {
	int a = 10; // instance variable
	static int b = 5; // static variable

	public void sample() {
		int c = 20; // local variable
		a = a + 5;
		System.out.println(a);
	}

	public static void main(String[] args) {
		Practice p = new Practice();
		Practice p1 = new Practice();
		//a = a + 20; // we cannot call a instance variable without creating a object
		//System.out.println(a);
		b = b - 5;
		System.out.println(b);// we can access static variable without using object
		//c = c * 2; // The scope of local variable is only in method
	}

}
