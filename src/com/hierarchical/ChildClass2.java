package com.hierarchical;

public class ChildClass2 extends ParentClass {
	private void childProperty2() {
		System.out.println("Property belongs to Child2 ");

	}

	public static void main(String[] args) {
		ChildClass2 c2 = new ChildClass2();
		c2.parentProeprty();
		c2.childProperty2();
	}

}
