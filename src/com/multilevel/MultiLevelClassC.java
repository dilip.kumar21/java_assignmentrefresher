package com.multilevel;

public class MultiLevelClassC extends MultiLevelClassB {
	private void collegeID() {
		System.out.println("College ID : 1525");
	}

	private void collegeName() {
		System.out.println("College Name : Sri Sairam");
	}

	public static void main(String[] args) {
		MultiLevelClassC C = new MultiLevelClassC();
		C.collegeID();
		C.collegeName();
		C.studentId();
		C.studentName();
		C.javaCourse();
		C.pythonCourse();
	}
}
