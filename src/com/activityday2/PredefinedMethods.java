package com.activityday2;

public class PredefinedMethods {
	public static void main(String[] args) {
		
		String s1= "Java";
		String s2="java";
		System.out.println(s1.charAt(0));
		System.out.println(s1.concat(s2));
		System.out.println(s1.contains(s2));
		System.out.println(s1.equalsIgnoreCase(s2));
		System.out.println(s1.toUpperCase());
		System.out.println(s1.toLowerCase());
		System.out.println(s1.isEmpty());
		System.out.println(s1.indexOf('a'));
		System.out.println(s1.replace('J', 'M'));
		System.out.println(s1.replaceAll(s1, "Welcome"));
	}


}
