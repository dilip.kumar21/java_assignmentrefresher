package com.activityday2;

import java.util.TreeSet;

public class TreeSetActivity {
	public static void main(String[] args) {
		TreeSet<String> T = new TreeSet<String>();
		TreeSet<String> t = new TreeSet<String>();
		T.add("Java");
		T.add("Python");
		T.add("Oracle");
		T.add("Aws");
		T.add("Azure");
		t.add("Selenium");
		System.out.println(T);
		for (String l : T) {
			System.out.println(l);

		}
		T.add("Azure");
		// T.add(null);

		System.out.println(T);
		System.out.println(T.first());
		System.out.println(T.last());

		t.addAll(T);// Adding all elements from one to another
		System.out.println(t);
		t.retainAll(T);// fetching common elements from both Tree set
		System.out.println(t);

	}

}
