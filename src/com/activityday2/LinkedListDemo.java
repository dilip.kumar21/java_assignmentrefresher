package com.activityday2;

import java.util.LinkedList;

public class LinkedListDemo {

	public static void main(String[] args) {
		LinkedList<String> li = new LinkedList<String>();
		li.add("a");
		li.add("b");
		li.add("c");
		li.add("d");
		li.add("e");
		li.add(null);// adding null in LinkedList
		li.add("e");// adding duplicate in LinkedList
		System.out.println(li);
		li.add(1, "a1");
		System.out.println(li);
		li.addFirst("A");
		li.addLast("Z");
		System.out.println(li);
		li.remove("d");
		System.out.println(li);

	}

}
//a points to a1  ,a1 points to b ,b points to c , c points to d,d points to e 