package com.activityday2;

import java.util.ArrayList;

public class ArrayListDemo {

	public static void main(String[] args) {
		ArrayList<String> a = new ArrayList<>();
		System.out.println(a.size());
		a.add("a");
		a.add("b");
		a.add("c");
		a.add("d");
		a.add("e");
		a.add("e"); // adding duplicate value in array list
		a.add(null);// adding null in array in array list
		// The element will stored in consecutive memory allocation
		System.out.println(a.size());
		a.add("f");
		System.out.println(a.size());
		// Replace the second element with B1
		a.set(1, "B1");

		System.out.println(a);
		a.remove(3);
		System.out.println(a);
		a.remove("e");
		System.out.println(a);

	}

}
