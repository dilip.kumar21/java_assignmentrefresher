package com.activityday2;

public class StringTopic {
	public static void main(String[] args) {
		String s1 = "Welcome";// String Pool Constant area
		char[] c = { 's', 'e', 'l', 'e', 'n', 'i', 'u', 'm' };
		String s2 = "Welcome";// String Pool Constant area
		String s3 = new String("Welcome");// Heap area
		String s4 = new String("Hello World");// Heap area
		String s5 = new String(c);// Heap area

		System.out.println(s1 + "\n" + s2 + "\n" + s3 + "\n" + s4 + "\n" + s5);
		s1.concat("Java");
		System.out.println(s1);
		s1=s1.concat("Java");
		System.out.println(s1);
	}

}
