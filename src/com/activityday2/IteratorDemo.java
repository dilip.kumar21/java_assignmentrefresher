package com.activityday2;

import java.util.ArrayList;
import java.util.Iterator;

public class IteratorDemo {
	public static void main(String[] args) {
		ArrayList<String> li = new ArrayList<String>();
		li.add("a");
		li.add("b");
		li.add("c");
		li.add("d");
		li.add("e");
		System.out.println(li);
		Iterator<String> it = li.iterator();
		while (it.hasNext()) {
			Object value = it.next();
			System.out.print(value+" ");
		}
	}

}
