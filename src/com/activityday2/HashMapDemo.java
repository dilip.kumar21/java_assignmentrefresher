package com.activityday2;

import java.util.Collection;
import java.util.HashMap;
import java.util.Set;

public class HashMapDemo {
	public static void main(String[] args) {
		HashMap<Integer, String> h = new HashMap<Integer, String>();
		h.put(1, "Java");
		h.put(2, "Python");
		h.put(3, "Selenium");
		h.put(4, "DevOps");
		System.out.println(h.get(4));
		System.out.println(h.size());
		h.put(4, "Dilip");

		System.out.println(h);
		Set<Integer> keys = h.keySet();
		System.out.println(keys);
		for (Integer inte : keys) {
			System.out.println(inte);
		}
		Collection<String> values = h.values();
		System.out.println(values);
		for (String st : values) {
			System.out.println(st);
		}

	}

}
