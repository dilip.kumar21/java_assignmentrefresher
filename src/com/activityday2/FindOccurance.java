package com.activityday2;

public class FindOccurance {

	public static void main(String[] args) {
		String s = "DilipKumar";
		char ch = 'i'; // character want to search 
		int count = 0;
		for (int i = 0; i < s.length(); i++) {
			if (s.charAt(i) == ch)
				count++;
		}
		System.out.println("Character '" + ch + "' appears " + count + " times.");

	}

}
