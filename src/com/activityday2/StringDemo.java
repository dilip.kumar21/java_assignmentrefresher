package com.activityday2;

public class StringDemo {

	public static void main(String[] args) {
		String s1 = "Welcome";
		String s2 = "Welcome";
		String s3 = new String("Welcome");
		String s4 = "Java";
		boolean st = s1.equals(s2);// true
		boolean st1 = s1.equals(s3);// true
		boolean st2 = s1.equals(s4);// false
		boolean st3 = s2.equals(s3);// true
		System.out.println(st + "\n" + st1 + "\n" + st2 + "\n" + st3);
		// equals() checks the content and not the reference
		boolean a1 = s1 == s2;// true
		boolean a2 = s1 == s3;// false
		boolean a3 = s1 == s4;// false
		boolean a4 = s2 == s3;// false
		System.out.println(a1 + "\n" + a2 + "\n" + a3 + "\n" + a4 + "\n");

	}

}
